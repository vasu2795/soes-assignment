use Mix.Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with brunch.io to recompile .js and .css sources.
config :ebach, Ebach.Endpoint,
  http: [port: 4000, compress: true],
  hostname: "http://localhost:4000",
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [node: ["node_modules/webpack/bin/webpack.js", "--progress", "--colors", "--watch",
                    cd: Path.expand("../", __DIR__)]]

# Watch static and templates for browser reloading.
config :ebach, Ebach.Endpoint,
  live_reload: [
    url: "ws://localhost:4000",
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif)$},
      ~r{web/views/.*(ex)$},
      ~r{web/templates/.*(eex)$}
    ]
  ]


{_, hostname} = :inet.gethostname


config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20
