var
  fs          = require('fs'),
  console      = require('better-console'),
  runSequence = require('run-sequence'),

  build       = require('./build'),
  // File destination configuration
  paths       = require('./config/user').paths.output;


/**
 * For each flavour copy varaibles to site.variables and build
 * css and js. Rename the files as with flavour prefix.
 */
module.exports = function (callback) {
  fs.readdir('./flavours', function (error, files) {
    if(error) {
      throw error;
    }

    var processFile = function (file) {
      if(!file) {
        console.info('Finished generating all flavoured CSS');
        callback();
        return;
      }

      flavour = file.split('.')[0];

      console.log(`Building flavour : ${flavour}`);

      fs.readFile(`./flavours/${file}`, 'utf-8', function (error, contents) {
        console.info(`Writing site.variables for ${flavour}`);

        fs.writeFile('./src/site/globals/site.variables', contents, 'utf-8', function (error, success) {
          if(error) {
            throw error;
          }

          console.info(`Building CSS for ${flavour}`);
          runSequence(['build-css'], function() {
            fs.readFile(`${paths.packaged}/semantic.css`, 'utf-8', function (error, generatedCss) {
              if(error) {
                throw error;
              }

              fs.writeFile(`${paths.flavours}/${flavour}.css`, generatedCss, function(error, status) {
                if(error) {
                  throw error;
                }

                console.info(`Generated CSS for ${flavour}`);
                processFile(files.shift());
              });
            });
          });
        });
      });
    };

    processFile(files.shift())
  });
};
