var
  fs          = require('fs'),
  runSequence = require('run-sequence'),
  console      = require('better-console'),

  build       = require('./build'),

  // File destination configuration
  paths       = require('./config/user').paths.output;



module.exports = function (callback) {
  fs.readFile(`${paths.packaged}/semantic.js`, 'utf-8', function (error, fileContents) {
    fs.writeFile('../priv/static/js/semantic.js', fileContents, function (error) {
      if(error) {
        console.error('Failed to publish semantic.js to webapp');
        throw error;
      }
    });
    
    fs.readdir(`${paths.flavours}`, function (error, files) {
      if(error) {
        console.error('Failed to get generated flavour files');
        throw error;
      }

      for(var i=0; i<files.length; i+=1) {
        var file = files[i];

        fs.readFile(`${paths.flavours}/${file}`, 'utf-8', function (error, fileContents) {
          if(error) {
            console.error('Failed to read flavour from generated build files');
            throw error;
          }

          fs.writeFile(`../priv/static/css/${file}`, fileContents, function(error) {
            if(error) {
              console.error(`Failed to publish ${file} to webapp`);
              throw error;
            }

            if(i == files.length - 1) {
              callback();
            }
          });
        });
      }
    });
  });
};
