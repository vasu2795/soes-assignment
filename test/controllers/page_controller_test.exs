defmodule Ebach.PageControllerTest do
  use Ebach.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert conn.status == 200
  end

  test "POST /soes/orders", %{conn: conn} do
    response = post conn,"/soes/orders", orders: [%{"company" => "XYZ", "type" => "Sell", "units" => "15"},
                                                   %{"company" => "ABC", "type" => "Sell", "units" => "13"},
                                                   %{"company" => "XYZ", "type" => "Buy", "units" => "10"},
                                                   %{"company" => "XYZ", "type" => "Buy", "units" => "8"}]
    {:ok, resp} =  Poison.decode(response.resp_body)
    IO.inspect "response is"
    assert response.status == 200
    assert resp["orders"] == [%{"company" => "XYZ", "status" => "CLOSED", "type" => "Sell", "units" => 0},
                             %{"company" => "ABC", "status" => "OPEN", "type" => "Sell", "units" => 13},
                             %{"company" => "XYZ", "status" => "CLOSED", "type" => "Buy", "units" => 0},
                             %{"company" => "XYZ", "status" => "OPEN", "type" => "Buy", "units" => 3}]
  end

end
