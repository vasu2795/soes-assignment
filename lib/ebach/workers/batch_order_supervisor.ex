# defmodule Ebach.BatchRefundSupervisor do
#   import Supervisor.Spec
#   use Supervisor
#
#   def start_link(arg) do
#     Supervisor.start_link __MODULE__, arg
#   end
#
#   def init(arg) do
#     children = [
#       worker(Ebach.BatchRefundWorker, []),
#       worker(Ebach.BatchRefundFSM, [])
#     ]
#
#     supervise(children, strategy: :one_for_all)
#   end
# end
