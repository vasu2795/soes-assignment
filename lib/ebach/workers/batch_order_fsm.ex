# defmodule Ebach.BatchRefundFSM do
#   require :gen_fsm
#   require Logger
#
#   @moduledoc """
#     FSM to represent the state of the node with respect to batch refund.
#   """
#
#   def start_link do
#     :gen_fsm.start_link({:global, __MODULE__}, __MODULE__, [], [])
#   end
#
#   def init(_) do
#     {:ok, :idle, %{}}
#   end
#
#   def send_event(event) do
#     pid = :global.whereis_name __MODULE__
#     if is_pid pid do
#       :gen_fsm.sync_send_event pid, event
#     end
#   end
#
#   @doc """
#     In idle state, the system does no work
#   """
#   def idle(event, from, state) do
#     case event do
#       :elect -> {:reply, :electing, :electing, state}
#       :RESET ->
#         {:reply, :idle, :idle, %{}}
#       _      ->
#         {:reply, :idle, :idle, state}
#     end
#   end
#
#   def electing(event, from, state) do
#     case event do
#       {:follow, leader} ->
#         {:reply, :waiting, :waiting, Map.merge(state, %{"following" => leader})}
#
#       :lead ->
#         {:reply, :working, :working, state}
#
#       :RESET ->
#           {:reply, :idle, :idle, %{}}
#       _   ->
#         {:reply, :electing, :electing, state}
#     end
#   end
#
#   @doc """
#     The system is waiting for an elected master to send it
#     instructions
#   """
#   def waiting(event, from, state) do
#     case event do
#       :workload_received ->
#         {:reply, :working, :working, state}
#
#       :RESET             ->
#         {:reply, :idle, :idle, %{}}
#       _                  ->
#         {:reply, :waiting, :waiting, state}
#     end
#   end
#
#   @doc """
#     The system is performing the refund tasks
#   """
#   def working(event, from, state) do
#     case event do
#       :finish -> {:reply, :idle, :idle, %{}}
#
#       :RESET  ->
#         {:reply, :idle, :idle, %{}}
#
#       _       ->
#         {:reply, :working, :working, state}
#     end
#   end
#
#   def terminate(cause, current_state, state) do
#     Logger.error "BatchRefundFSM exited due to #{cause}"
#   end
# end
