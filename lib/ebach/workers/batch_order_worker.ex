# defmodule Ebach.BatchRefundWorker do
#   use GenServer
#   import Ecto.{Query, Changeset}
#   import Logger
#   # alias Ebach.{Repo, BatchRefundEntry, MerchantAccount, BatchRefundFSM}
#
#   @refund_api_url "#{Application.get_env(:ebach, Ebach.Endpoint)[:environment]}/order/refund"
#
#   @doc """
#     Link both the GenServer and the batch refund FSM here an make them
#     dependent on each other
#   """
#   def start_link(state \\ nil, args \\ []) do
#     GenServer.start_link(__MODULE__, state, args)
#   end
#
#   def handle_call(data, from, state) do
#     {:reply, [], state}
#   end
#
#   def init(state) do
#     decide_leader Node.list
#     {:ok, state}
#   end
#
#   @doc """
#     Decides a leader based upon the list of available nodes.
#     The leader is selected by taking a list of all nodes in the distribution and
#     creating a sorted list out of the nodes.
#
#     The first entry of the node is selected to be the leader.
#     This approach is taken as the results will be reproducible across all nodes.
#
#     If the list of available nodes is empty, skip refund processing and try again in
#     the next iteration.
#   """
#   def decide_leader(nodes \\ [], iteration \\ 0) do
#     unless length(nodes) == 0 do
#       case BatchRefundFSM.send_event :elect do
#         :electing ->
#           Logger.info "Deciding leader to coordinate batch refund process"
#
#           self = Atom.to_string(Node.self)
#           others = Enum.map(nodes, &Atom.to_string/1)
#           nodes = Enum.concat(others, [self]) |> Enum.sort
#           leader = hd(nodes)
#
#           if self == leader do
#             Logger.info "<#{self}> is the leader, assigning refunds to peers"
#
#             BatchRefundFSM.send_event :lead
#             assign_refund_workload(others)
#           else
#             BatchRefundFSM.send_event {:follow, leader}
#             Logger.info "#{self} is following #{leader}, awaiting instructions"
#           end
#
#         :working ->
#           Logger.info "Skipping leader election process, still working"
#
#         :waiting ->
#           Logger.info "Skipping leader election process, waiting for leader to assign tasks"
#
#         _ -> Logger.info "Got event : #{BatchRefundFSM.send_event :elect}"
#       end
#     else
#       Logger.warn "No nodes reachable from #{Node.self}, do leader election in next iteration #{iteration + 1}"
#     end
#
#
#     :timer.sleep(10000)
#
#     decide_leader Node.list, iteration + 1
#   end
#
#   @doc """
#     Leader assigns the workloads to the various nodes in the cluster and finally
#     to itself.
#   """
#   def assign_refund_workload(nodes) do
#     Logger.info "#{Node.self} is assigning refund workloads."
#
#     base_query = BatchRefundEntry |> where([b], b.status == "PENDING" or b.status == "REQUEST_FAILED")
#
#     {first} = base_query |> first |> select([b], {b.id}) |> order_by([b], b.id) |> Repo.one
#     {last} = base_query |> last |> select([b], {b.id}) |> order_by([b], b.id) |> Repo.one
#
#     node_count = length(nodes) + 1
#     refunds_count = last - first + 1
#
#     # Number of refunds to be processed per node
#     per_node_count = div(refunds_count - rem(refunds_count, node_count), node_count) + 1
#
#     Logger.info "#{refunds_count} pending processing, #{per_node_count} refunds will be processed per node"
#
#     Enum.map(
#       nodes,
#       fn node ->
#         Logger.info "Assigning refunds from #{first} to #{first + per_node_count} to #{node}"
#         Task.Supervisor.async(
#           {Ebach.BatchRefundSupervisor, String.to_atom(node)},
#           Ebach.BatchRefundWorker,
#           :do_assigned_workload, [first, per_node_count])
#
#         first = first + per_node_count + 1
#       end)
#
#     process_batch_refunds(first, per_node_count)
#
#     BatchRefundFSM.send_event :finish
#   end
#
#   @doc """
#     Update node state and process refunds and :finish
#   """
#   def do_assigned_workload(start, count) do
#     case BatchRefundFSM.send_event :workload_received do
#       :working ->
#         Logger.info "#{Node.self} Starting refund process for #{count} refunds starting from #{start}"
#
#         process_batch_refunds start, count
#         BatchRefundFSM.send_event :finish
#
#       _ ->
#         Logger.error "Not doing assigned workload, not yet ready for work"
#     end
#   end
#
#   @doc """
#     Process refunds having status PENDING or REQUEST_FAILED
#     starting from the offset record, processing the number of
#     records specified in the count argument
#   """
#   def process_batch_refunds(start, count) do
#     Logger.info "Collecting information to start refund process"
#
#     refunds = Repo.all(
#       from r in BatchRefundEntry,
#       where: r.status == "PENDING" or r.status == "REQUEST_FAILED",
#       limit: ^count,
#       offset: ^start
#     )
#
#     Enum.reduce(refunds, 1, fn refund, ind ->
#       Logger.info "Starting refund #{ind} of #{count}"
#       case call_refund_api(refund)  do
#         {:ok, response} ->
#           Logger.info "Got refund response from #{@refund_api_url}", [response]
#           response_text = response.body
#           {status, response} = Poison.decode(response_text)
#           if response["status"] == "invalid_request_error" do
#             update_refund_status(refund, "INVALID_REQUEST", response_text)
#           else
#             refund_response = Enum.find(response["refunds"], fn rf -> rf["unique_request_id"] == refund.unique_request_id end)
#             if refund_response != nil do
#               update_refund_status(refund, refund_response["status"])
#             else
#               Logger.error "Could not find refund #{refund.unique_request_id} in response from #{@refund_api_url}"
#               update_refund_status(refund, "REQUEST_FAILED")
#             end
#           end
#         {:error, _} ->
#           Logger.warn "Call to the refund API #{@refund_api_url} failed for #{refund.order_id}. Marking as REQUEST_FAILED"
#           update_refund_status(refund, "REQUEST_FAILED")
#       end
#
#       ind = ind + 1
#     end)
#   end
#
#   @doc """
#     Call the refund api to initate the actual refund
#   """
#   defp call_refund_api(%{order_id: order_id, amount: amount, unique_request_id: unique_request_id} = refund) do
#     auth_header = MerchantAccount.generate_basic_auth(get_merchant_id(refund))
#
#     Logger.info "Starting refund for batch #{order_id} with unique_request_id #{unique_request_id}", [refund]
#
#     HTTPoison.post(
#       @refund_api_url,
#       {:form, [{"order_id", order_id}, {"amount", "#{amount}"}, {"unique_request_id", unique_request_id}]},
#       [{"Authorization", "Basic #{auth_header}"}],
#       [{:timeout, 30000}])
#   end
#
#   @doc """
#     Update the refund status
#   """
#   defp update_refund_status(refund, status, response_text \\ nil) do
#     {status, data} = refund
#       |> cast(%{"status": status, "response_text": response_text}, [:status, :response_text])
#       |> Repo.update
#
#     case status do
#       :ok -> Logger.info "Marking #{refund.unique_request_id} as #{status}"
#       :error -> Logger.error("Failed to mark #{refund.unique_request_id} as #{status}")
#     end
#
#     data
#   end
#
#   @doc """
#     Helper method to get the merchant_id given a refund entry
#   """
#   defp get_merchant_id(refund_entry) do
#     refund_entry
#       |> Repo.preload(:batch_refund)
#       |> Map.get(:batch_refund)
#       |> Repo.preload(:merchant_account)
#       |> Map.get(:merchant_account)
#       |> Map.get(:merchant_id)
#   end
# end
