export default function navigateTo(state={}, action) {
  switch(action.type) {
    case 'NAVIGATION_ACTION':
      return Object.assign({}, state, {
        lastVisited: action.to
      });
    default:
      return state;
  }
}
