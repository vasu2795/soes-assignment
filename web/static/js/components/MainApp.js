import React from 'react';
import R from 'ramda';
import Sidebar from './navigation/Sidebar';
import NavInfoModal from './navigation/NavInfoModal';
import dispatchNavigationEvent from '../RoutingManagement';

export default class MainApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.prepareNavigationItems()
    };
  }

  prepareNavigationItems() {
    let root = "SOES Problem"
    let items = [];
    let thisItems = [ {
          category: 'SOES',
          regex: /^soes\/orders$/i,
          template: `soes / orders`,
          description: 'Create Stock Orders'
        }];

    items.push(...thisItems);
    items = R.pipe(
      R.uniqBy(R.prop('template'))
    )(items);
    return items;
  }

  loadHome() {
    let location = R.pipe(R.split('/'), R.map(R.trim), R.reject(R.isEmpty), R.join('/'))(this.state.items[0].template);
    dispatchNavigationEvent(location);
  }

  componentDidMount() {
    this.loadHome();
  }

  render() {
    return (
      <div id="container">
        <Sidebar navHelper={this.refs.navHelper} items={this.state.items} />
        <div style={{
          marginBottom: '50px'
        }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}
