import React from 'react';
import R from 'ramda';
import store from '../../store/AppStore';
import dispatchNavigationEvent from '../../RoutingManagement';

export default class NavInfoModal extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      navRequest: {
        expect: []
      },
      expectValues: {},
      formattedQuery: '',
      modalInstance: null,
      promise: {
        resolve: null,
        reject: null
      }
    };
  }

  componentDidMount() {
    this.state.modalInstance = $('#nav-info-modal').modal();
    this.state.modalInstance.onHidden = () => {
      if(typeof this.state.promise.reject == 'promise') {
        this.state.promise.reject();
      }
    };

    this.setState(this.state);
  }

  requestNavigationInfo(navRequest) {
    this.state.expectValues = {};
    this.state.navRequest = navRequest;
    this.state.formattedQuery = navRequest.template;

    this.setState(this.state);
    R.map((expect) => { this.state.expectValues[expect]=""; return true;},this.state.navRequest.expect); // set initial value ""

    this.state.modalInstance.modal('show');

    return new Promise((resolve, reject) => {
      this.state.promise = {
        resolve: resolve,
        reject: reject
      };

      this.setState(this.state);
    });
  }

  formatQueryInput(template, value) {
    this.state.expectValues[template] = value;
    this.state.formattedQuery = R.reduce((query, templateObj) => {
      return R.replace(templateObj[0], templateObj[1], query);
    }, this.state.navRequest.template, R.toPairs(this.state.expectValues));
    this.setState(this.state);
  }

  goToLocation(ev) {
    ev.nativeEvent.preventDefault();
    this.state.modalInstance.modal('hide');
    if(typeof this.state.promise.resolve === 'function') {
      this.state.promise.resolve();
    }
    dispatchNavigationEvent(this.state.formattedQuery.match(/(\S+)/ig).join(''));
  }

  render() {
    let navRequest = this.state.navRequest;
    return (
      <div className="ui small modal" id="nav-info-modal">
        <div className="header">Enter Details</div>
        <div className="content">
          <form className="ui form" onSubmit={(ev) => ::this.goToLocation(ev)}>
            {R.map((pathField) => {
                              return (
                  <div className="field">
                    <div className="ui labeled input">
                      <div className="ui label">{pathField}</div>
                      <input
                        required
                        value={this.state.expectValues[pathField]}
                        onChange={(ev) => ::this.formatQueryInput(pathField, ev.nativeEvent.target.value)} />
                    </div>
                  </div>
                );
            }, navRequest.expect)}
            <button type="submit" className="ui primary button">Go</button>
          </form>
        </div>
      </div>
    );
  }
}
