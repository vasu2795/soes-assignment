import React from 'react';
import R from 'ramda';
import axios from 'axios';
import dispatchNavigationEvent from '../../RoutingManagement';
import Dropzone from 'react-dropzone';

class BatchOrderConfirmationModal extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      modalInstance: null,
      batches: []
    };
  }

  componentDidMount() {
    $('#batch-order-confirmation-modal-tabs .item').tab();
    this.state.modalInstance = $('#batch-order-confirmation-modal').modal();
    this.setState(this.state);
  }

  open() {
    this.state.modalInstance.modal('show');
  }

  startProcess() {
    let batches = this.state.batches;
    console.log("starting process with ",batches[0].orders);
    console.log("batches is ",batches);
      axios.post(`soes/orders`, {orders: batches[0].orders}).then((response) => {
      batches.pop();
      console.log("response is ",response);
      let orders = response.data.orders;
      this.props.onRefundStart(orders);
      this.state.modalInstance.modal('hide');
    }).catch((error) => {
      console.log(error);
    });
  }

  // removeFile(file) {
  //   this.state.batches = R.reject(R.propEq('file_name', file.file_name), this.state.batches);
  //   this.setState(this.state);
  // }

  render() {
    let files = this.state.batches;
    let processFile = (fileText, file) => {
      fileText = R.pipe(R.split('\n'), R.map(R.trim), R.reject(R.isEmpty))(fileText);
      fileText = R.map((stockOrder) => {
        stockOrder = R.pipe(R.split(','), R.map(R.trim), R.reject(R.isEmpty))(stockOrder);
        console.log("stock order is ",stockOrder);
        return {stock_id: stockOrder[0],type: stockOrder[1], company: stockOrder[2], units: stockOrder[3]};
      }, fileText);
      fileText = R.drop(1,fileText)
      this.state.batches.push({
        orders: fileText,
      });
      this.startProcess();
      this.setState(this.state);
      $('#batch-order-confirmation-modal-tabs .item').tab();
    };

    return (
      <div className="ui large modal" id="batch-order-confirmation-modal">
        <div className="header">Create Batch Order</div>
        <div className="content">
          <div className="ui top pointing secondary menu" id="batch-order-confirmation-modal-tabs">
            <form className="ui form">
              <Dropzone onDrop={(files) => {
                let reader = new FileReader();
                reader.onload = () => {
                  processFile(reader.result, files[0]);
                };

                reader.readAsText(files[0]);
              }}

              style={{
               width: '100%',
               height: '80%',
               textAlign: 'center',
               padding: '90px',
               border: '1px dashed black',
               borderRadius: '3px'
              }}
              >
                <div className="ui heading">
                  Click here / Drop a file here
                </div>
              </Dropzone>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default class OrderList extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      list: []
    };
  }

  componentDidMount() {
    this.loadRefunds();
  }

  loadRefunds() {
    let merchantId = this.props.routeParams.merchant_id;

  }

  render() {
    let merchantId = this.props.routeParams.merchant_id;
    let orders = this.state.list;
    const datas = [{
      stock_id: 1,
      type: 'Buy',
      company: 'ABC',
      units: 10
    },{
      stock_id: 2,
      type: 'Sell',
      company: 'XYZ',
      units: 15
    },{
      stock_id: 3,
      type: 'Sell',
      company: 'ABC',
      units: 13
    },
    {
      stock_id: 4,
      type: 'Buy',
      company: 'XYZ',
      units: 10
    },{
      stock_id: 5,
      type: 'Buy',
      company: 'XYZ',
      units: 8
    }];
    const columns = [{
        id: 'stock_id',
        displayName: 'Stock Id'
      }, {
        id: 'type',
        displayName: 'Side'
      },{
        id: 'company',
        displayName: 'Company'
    }, {
      id: 'units',
      displayName: 'Quantity'
    }];
    let onRefundStart = (orders) => {
      console.log("here ",orders);
      this.state.list = orders;
      console.log("this state list is ",this.state.list);
      this.setState(this.state);
    };
    return (
      <div className="ui basic segment enterFrame">
        <BatchOrderConfirmationModal onRefundStart={onRefundStart} merchantId={merchantId} ref="createBatch" />
        <div className="ui basic segment">
          {do {
            if(this.state.list && this.state.list.length > 0) {[
              <table className="ui basic padded selectable segment table">
                <thead >
                  <tr>
                    <th>Stock Id</th>
                    <th>Type</th>
                    <th>Company</th>
                    <th>Units</th>
                    <th>Remaining</th>
                    <th>Status </th>
                  </tr>
                </thead>
                <tbody>
                  {R.addIndex(R.map)((order, index) => {
                    console.log("inside listing ",order);
                    return (
                      <tr>
                        <td>{order.stock_id}</td>
                        <td>{order.type}</td>
                        <td>{order.company}</td>
                        <td>{order.units}</td>
                        <td>{order.remaining}</td>
                        <td>{order.status}</td>
                      </tr>
                    )
                  }, orders)}
                </tbody>
              </table>,
              <button  onClick={(ev) => this.refs.createBatch.open()} className="ui right floated green button">Create new Stock Order</button>
            ]} else {
              <div className="ui success message">
                <div className="header">Initiate a stock order</div>
                Click <a onClick={(ev) => this.refs.createBatch.open()}>here</a> to create a new Stock Order
              </div>
            }
          }}
        </div>
      </div>
    );
  }
}
