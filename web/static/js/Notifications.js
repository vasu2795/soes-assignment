import { Socket } from 'phoenix';

let socket = new Socket('/socket', {
	logger: (kind, msg, data) => {}
});

socket.connect();

export default socket;
