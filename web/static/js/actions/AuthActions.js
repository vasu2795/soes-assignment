export function initApp() {
  return {
    type: 'INIT'
  };
}

export function teardownApp() {
  return {
    type: 'TEARDOWN'
  };
}
