// import "phoenix_html";

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import ReactDOM from 'react-dom';
import React from 'react';
import axios from 'axios';

import store from './store/AppStore';
import { initApp } from './actions/AuthActions';
import MainApp from './components/MainApp';
import OrderList from '../js/components/order/OrderList';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';


axios.defaults.headers.common['Version'] = '2016-07-19';

let currentUrl = window.location.href;

if(!currentUrl.includes("/reset")){
    hashHistory.push('/app');
}

hashHistory.listen((args) => {
  let simplePath = args.pathname.split('/').join('');
  let currentSimplePath = store.getState()['Navigation'].lastVisited.split('/').join('');

  if(simplePath != currentSimplePath) {
		/*
			Do not call dispatchNavigationEvent here.
			Dispatch navigation event will update the router history ->
			and the snake eats itself.
		 */
    store.dispatch({
      type: 'NAVIGATION_ACTION',
      to: args.pathname
    });
  }
});

ReactDOM.render(
	<Router history={hashHistory}>
    <Route path="/app" component={MainApp}>
      <Route path="/soes/orders" component={OrderList} />
    </Route>
	</Router>, document.getElementById('application'));
