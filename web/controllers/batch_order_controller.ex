defmodule Ebach.BatchOrderController do
  use Ebach.Web, :controller
  # alias Guardian.Plug.{EnsureAuthenticated, EnsurePermissions}
  # plug EnsureAuthenticated

  def loop(head,middle,tail) do
    cond do
      # Handle already CLOSED orders
      Map.get(head,"status") == "CLOSED" ->
                        [new_head | new_tail] = middle ++ tail;
                        [head] ++ loop(new_head,[],new_tail);

      # Handle end of iteration
      tail == [] -> [head] ++ middle;

      true ->
        [next| other] = tail;
        {status,updated_hd,updated_next} = match_orders(next,head)
        case status do
          :success ->
             [new_head | new_tail] = middle ++ [updated_next| other];
             [updated_hd] ++ loop(new_head,[],new_tail);
          :failure ->
             middle = List.insert_at(middle,-1,updated_next);
             loop(updated_hd,middle,other);
        end
    end
  end

  def match_orders(next,hd) do
    case ((next["company"] == hd["company"]) && not(next["type"] == hd["type"]))  do
      true ->
          diff = (hd |> Map.get("remaining")) - (next |>Map.get("remaining"))
                cond do
                  diff > 0  -> {:failure,Map.merge(hd, %{"remaining" => abs diff}), Map.merge(next,%{"status" => "CLOSED","remaining" => 0})}
                  diff == 0 -> {:success,Map.merge(hd,%{"status" => "CLOSED", "remaining" => 0}),Map.merge(next,%{"remaining" => 0,"status" => "CLOSED"})}
                  diff < 0  -> {:success,Map.merge(hd,%{"status" => "CLOSED", "remaining" => 0}),Map.merge(next,%{"remaining" => abs diff})}
                end
      false -> {:failure,hd,next}
    end
  end


  def create_batch_orders(conn,%{"orders" => orders}) do
    orders = Enum.map(orders, fn x -> Map.merge(x,%{"status" => "OPEN", "units" => String.to_integer(x["units"]), "remaining" => String.to_integer(x["units"])}) end );
    [hd|tail] = orders;
    json conn,%{"orders" => loop(hd,[],tail)}
  end

end
