defmodule Ebach.PageController do
  use Ebach.Web, :controller

  def ping(conn, params) do
    name = Node.self
    json conn, %{"name": name}
  end

  @doc """
    Return the index page of the app.
  """
  def index(conn, _params) do
    stylesheet =  "semantic.css"
    render conn, "index.html", stylesheet: stylesheet
  end
end
