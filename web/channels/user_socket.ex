defmodule Ebach.UserSocket do
  use Phoenix.Socket

  # channel "notification:*", Ebach.NotificationsChannel

  transport :websocket, Phoenix.Transports.WebSocket

  def connect(_params, socket) do
    {:ok, socket}
  end

  def id(_socket), do: nil
end
