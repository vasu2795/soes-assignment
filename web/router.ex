defmodule Ebach.Router do
  use Ebach.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json", "html"]
    plug :fetch_session
    plug :put_secure_browser_headers
    # plug Guardian.Plug.VerifySession
    # plug Guardian.Plug.LoadResource
  end


  scope "/", Ebach do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/ping", PageController, :ping
  end

  scope "/soes", Ebach do
    pipe_through :api
    # BatchOrder API
    post "/orders", BatchOrderController, :create_batch_orders
  end

end
