use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: :dev

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html


# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile
environment :dev do
  set dev_mode: false
  set include_erts: false
  set include_src: false
  set vm_args: "rel/vm.args"
  set cookie: :"@?~-y,Q~v:z3%?G]8cpr4>6)B+sG[?bMXugc$KC*:@?/E2,2Fh%Fyx+uYn~S+$$w"
end

environment :sandbox do
  set dev_mode: false
  set include_erts: true
  set include_src: false
  set cookie: :"@?~-y,Q~v:z3%?G]8cpr4>6)B+sG[?bMXugc$KC*:@?/E2,2Fh%Fyx+uYn~S+$$w"
end

environment :prod do
  set include_erts: true
  set include_src: false
  set vm_args: "rel/vm.args"
  set cookie: :"@?~-y,Q~v:z3%?G]8cpr4>6)B+sG[?bMXugc$KC*:@?/E2,2Fh%Fyx+uYn~S+$$w"
end

# You may define one or more releases in this file.
# If you have not set a default release, or selected one
# when running `mix release`, the first release in the file
# will be used by default

release :ebach do
  set version: current_version(:ebach)
end
