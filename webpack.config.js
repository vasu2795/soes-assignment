const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const DefinePlugin = require('webpack').DefinePlugin;
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  devtool: "source-map",
  entry: [__dirname + "/web/static/js/app.js", __dirname + "/web/static/css/app.css"],
  output: {
    path: __dirname + "/priv/static/js",
    filename: "app.js"
  },
  resolve: {
    modules: [ "node_modules", __dirname + "/web/static/js" ]
  },
  module: {
    rules: [
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader?stage=0"
      },
      {
        test: /\.css$/,
        loader: "css-loader"
      },
      {
        test: /\.png$/,
        loader: "url-loader"
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: "file"
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("css/app.css"),
    new CopyWebpackPlugin([{ from: "./web/static/assets" }]),
    new DefinePlugin({
      'process.env': {
        // This has effect on the react lib size
        'NODE_ENV': JSON.stringify('producstion')
      }
    }),
    new UglifyJSPlugin({sourceMap: true})
  ]
};
